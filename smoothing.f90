module smoothing
  implicit none 
contains 

  subroutine triangle_2d(meas,wav,wav_smooth,meas_smooth)
    
    real(kind=4), dimension(:), intent(in) :: wav
    real(kind=4), dimension(:,:), intent(in) :: meas
    real(kind=4), dimension(:), intent(in) :: wav_smooth
    integer(kind=4) :: n_sample, n_wav , n_smooth, i, j, k 
    real(kind=4), dimension(size(meas,1),size(wav_smooth)), intent(out) :: meas_smooth
    real(kind=4), parameter :: fwhm = 2.5
    real(kind=4) :: b
    real(kind=4) :: sum1
    
    n_sample = size(meas,1)
    n_wav = size(wav)
    n_smooth = size(wav_smooth)

    meas_smooth = 0.

    do i = 1, n_sample
       do j = 1, n_smooth
          sum1 = 0
          do k=1, n_wav

             if (abs(wav(k)-wav_smooth(j)) < fwhm) then
                
                b=1.0-abs(wav(k)-wav_smooth(j))/fwhm ! triangular weight

                meas_smooth(i,j) = meas_smooth(i,j) + b*meas(i,k) ! use all measurements
                sum1= sum1 + b ! sum of weights

             endif
          enddo
          meas_smooth(i,j)=meas_smooth(i,j)/sum1
       enddo
    enddo
     end subroutine triangle_2d
  
  subroutine triangle_1d(meas,wav,wav_smooth,meas_smooth)

    real(kind=4), dimension(:), intent(in) :: wav, meas
    real(kind=4), dimension(:), intent(in) :: wav_smooth
    integer(kind=4) :: n_wav, n_smooth 
    real(kind=4), dimension(size(wav_smooth)), intent(out) :: meas_smooth
    real(kind=4), parameter :: fwhm = 2.5
    real(kind=4) :: b
    integer(kind=4) :: i, j
    real(kind=4) :: sum1

    n_wav = size(wav)
    n_smooth = size(wav_smooth)

    meas_smooth(:) = 0.

    do i = 1, n_smooth
       sum1 = 0
       do j=1, n_wav
          if (abs(wav(j)-wav_smooth(i)) < fwhm) then
             b=1.0-abs(wav(j)-wav_smooth(i))/fwhm ! triangular weight
             meas_smooth(i) = meas_smooth(i) + b*meas(i) ! use all measurements
             sum1= sum1 + b ! sum of weights
          endif
       enddo
       meas_smooth(i)=meas_smooth(i)/sum1
    enddo
  end subroutine triangle_1d

end module smoothing
       


