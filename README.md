This is a set of fortran subroutines for smoothing spectral data with a triangular bandpass which can be used in python with the .so library file. T

The numpy library was simply compiled with the following command:

python -m numpy.f2py -c smoothing.f90 -m smoothing

There are two subroutines, triangle_1d and triangle_2d, the former does smoothing for a single spectra while the later smooths an array of input spectra 

sample usage in python:

```
import smoothing

smooth_spectra = smoothing.smoothing.triangle_1d(inp_spectra,inp_wavelength,smooth_wavelength)

#where
#inp_spectra, inp_wavelength, smooth_spectra, and smooth wavelength are a 1-d arrays with dimension (num_wavelength)

smooth_spectra = smoothing.smoothing.triangle_2d(inp_spectra,inp_wavelength,smooth_wavelength)

#where
#inp_spectra and smooth_spectra are 2-d arrays with dimensions (num_spectra,num_wavelength)
#inp_wavelength and smooth_wavelength are a 1-d arrays with dimension (num_wavelength)
```



